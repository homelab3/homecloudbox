docker network create proxy
docker-compose \
    -f tools/traefik/compose-traefik.yml \
    -f tools/compose-portainer.yml \
    -f apps/whoogle/compose-whoogle.yml \
    up -d
