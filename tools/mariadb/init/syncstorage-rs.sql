CREATE DATABASE IF NOT EXISTS syncstorage_rs; 
CREATE DATABASE IF NOT EXISTS tokenstorage_rs; 
CREATE USER 'sync_rs'@'%' IDENTIFIED BY 'password';
GRANT ALL ON syncstorage_rs.* TO 'sync_rs'@'%' IDENTIFIED BY 'password' WITH GRANT OPTION;
GRANT ALL ON tokenstorage_rs.* TO 'sync_rs'@'%' IDENTIFIED BY 'password' WITH GRANT OPTION;
FLUSH PRIVILEGES;
