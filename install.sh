#!/bin/bash
# Detect package manager
declare -A osInfo;
osInfo[/etc/debian_version]="apt-get install -y"
osInfo[/etc/arch-release]="pacman -S"
osInfo[/etc/centos-release]="yum install -y"
osInfo[/etc/fedora-release]="dnf install -y"

for f in ${!osInfo[@]}
do
    if [[ -f $f ]];then
        package_manager=${osInfo[$f]}
    fi
done

## Needed to run the dialog box
if ! command -v dialog &> /dev/null
then
    sudo ${package_manager} dialog
fi

#####################################################
#                                                   #
# Welcome message box                               #
#                                                   #
#####################################################
HEIGHT=15
WIDTH=40
TITLE="Welcome to HomeCloud Installation"
MSG="\n
This Installer will guide you through the \
process of installing a complete HomeCloud \
environment. \n \n
USE THIS SOFTWARE AT YOUR OWN RISK!"

dialog --clear \
    --title "$TITLE" \
    --msgbox "$MSG" \
    $HEIGHT $WIDTH 

#####################################################
#                                                   #
# Application selection checklist                   #
#                                                   #
#####################################################

HEIGHT=15
WIDTH=40
CHOICE_HEIGHT=8
TITLE="HomeCloud Application"
MENU="Choose which application to install:"

OPTIONS=(1 "Bitwarden" off \
         2 "Code Server" off \
         3 "Firefox SyncServer" off \
         4 "Nextcloud" off \
         5 "OnlyOffice" off \
         6 "Portainer" off \
         7 "Watchtower" off \
         8 "Adminer" off \
         9 "Joplin Server" off)

APPLICATIONS=$(dialog --clear \
                --title "$TITLE" \
                --checklist "$MENU" \
                $HEIGHT $WIDTH $CHOICE_HEIGHT \
                "${OPTIONS[@]}" \
                2>&1 >/dev/tty)

clear

echo "#####################################################"
echo "#                                                   #"
echo "#             Prerequisite installation             #"
echo "#                                                   #"
echo "#####################################################"

if ! command -v docker &> /dev/null
then
    sudo ${package_manager} docker.io
fi

if ! command -v docker-compose &> /dev/null
then
    sudo ${package_manager} docker-compose
fi

sudo usermod -a -G docker $USER


echo "#####################################################"
echo "#                                                   #"
echo "#              Applications installation            #"
echo "#                                                   #"
echo "#####################################################"


MARIADB=
POSTGRES=
BITWARDEN=
CODE_SERVER=
FIREFOX_SYNCSERVER=
NEXTCLOUD=
ONLYOFFICE=
PORTAINER=
WATCHTOWER=
ADMINER=
JOPLIN=


for value in $APPLICATIONS
do
    case $value in
    1)
        BITWARDEN=true
        ;;
    2)
        CODE_SERVER=true
        ;;
    3)
        FIREFOX_SYNCSERVER=true
        ;;
    4)
        NEXTCLOUD=true
        POSTGRES=true
        ;;
    5)
        ONLYOFFICE=true
        POSTGRES=true
        ;;
    6)
        PORTAINER=true  
        ;;
    7)
        WATCHTOWER=true
        ;;
    8)
        ADMINER=true
        ;;
    9)
        JOPLIN=true
        POSTGRES=true
        ;;
    *)
        echo ""
        ;;
    esac
done
docker network create proxy
docker network create db 
[ $MARIADB ] && docker-compose -f tools/mariadb/compose-mariadb.yml up -d 
[ $POSTGRES ] && ( docker-compose -f tools/postgres/compose-postgres.yml up -d && docker-compose -f tools/compose-adminer.yml up -d )
[ $BITWARDEN ] && docker-compose -f apps/compose-bitwarden.yml up -d
[ $CODE_SERVER ] && docker-compose -f apps/compose-codeserver.yml up -d
[ $FIREFOX_SYNCSERVER ] && docker-compose -f apps/compose-syncserver.yml up -d
[ $NEXTCLOUD ] && docker-compose -f apps/compose-nextcloud.yml up -d
[ $ONLYOFFICE ] && docker-compose -f apps/compose-onlyoffice.yml up -d
[ $PORTAINER ] && docker-compose -f tools/compose-portainer.yml up -d
[ $WATCHTOWER ] && docker-compose -f tools/compose-watchtower.yml up -d
[ $ADMINER ] && docker-compose -f tools/compose-adminer.yml up -d
[ $JOPLIN ] && docker-compose -f apps/compose-joplin-server.yml up -d



#####################################################
#                                                   #
#               End message box                     #
#                                                   #
#####################################################
HEIGHT=15
WIDTH=40
TITLE="Welcome to HomeCloud Installation"
MSG="\n
Congratulation you have successfully installed you HomeCloudBox!"

dialog --clear \
    --title "$TITLE" \
    --msgbox "$MSG" \
    $HEIGHT $WIDTH 
