CREATE DATABASE confluence;
CREATE USER confluenceuser WITH ENCRYPTED PASSWORD 'password';
GRANT ALL PRIVILEGES ON DATABASE confluence TO confluenceuser;
